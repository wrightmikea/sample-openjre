FROM alpine:3.9.4
RUN mkdir -p /usr/share/man/man1 && \
    apk --update add java-cacerts openjdk8-jre 
CMD ["/usr/bin/java", "-version"]
